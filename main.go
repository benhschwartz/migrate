package main

import (
	"database/sql"
	"fmt"
	"log"

	"bitbucket.org/benhschwartz/migrate/pkg/migrations"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/spf13/viper"
)

func init() {
	viper.AutomaticEnv()
	viper.SetDefault("MIGRATION_DIR", "/app/migrations")
}

func main() {
	dbUser := viper.GetString("DB_USER")
	dbPassword := viper.GetString("DB_PASSWORD")
	dbHost := viper.GetString("DB_HOST")
	dbPort := viper.GetString("DB_PORT")
	dbName := viper.GetString("DB_NAME")
	dbSslMode := viper.GetString("DB_SSL_MODE")
	migrationDsn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s", dbUser, dbPassword, dbHost, dbPort, dbName, dbSslMode)

	migrationDir := viper.GetString("MIGRATION_DIR")

	log.Print("Running migrations")

	db, err := sql.Open("pgx", migrationDsn)
	if err != nil {
		log.Fatalf("Problem running dbx migrations... %v", err)
	}
	defer db.Close()
	if err := db.Ping(); err != nil {
		log.Fatalf("could not ping database... %v", err)
	}

	if err := migrations.RunMigrations(db, migrationDir); err != nil {
		log.Fatalf("Problem running dbx migrations... %v", err)
	}
	log.Print("Migrations completed successfully")
}
